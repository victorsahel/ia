from kanren import run, var
from kanren import Relation, facts

M = var()
P = var()
H = var()

Esposo = Relation()
Hijo = Relation()

facts(Esposo,
      ("sabino","rosalia"),
      ("david","flor"),
      ("rene","nelith")
    )

facts(Hijo,
      ("david","sabino"),
      ("rene","sabino"),
      ("hernan","sabino"),
      
      ("angie","david"),
      ("marcelo","rene"),
    )
Dato = "rosalia"
A = run(0, (H,Dato), Hijo(H,P), Esposo(P,Dato))        
print(A)
