import numpy
import random


def Parejas(N):
    Aleatorio = random.sample(range(int(N / 2),N), int(N/2))
    Pareja={}
    for i in range(int(N/2)):
        Pareja[i] = Aleatorio[i]
        Pareja[Aleatorio[i]] = i
    return Pareja
def Inicializacion(N):
    global Individuos
    Individuos = {}
    for i in range(N):
        Individuos[i] = numpy.random.choice(range(1,N+1),N,replace=True)
    Mostrar(N)

def Seleccion(N):
    Pareja = Parejas(N)
    for k,v in Pareja.items():
        if Idoneidad(Individuos[k],N) >= Idoneidad(Individuos[v],N):
            Individuos[v] = Individuos[k]
        else:
            Individuos[k] = Individuos[v]
    Mostrar(N)


def Idoneidad(Tablero, N):
    Atacadas = 0
    for i in range(len(Tablero)):
        for j in range(i+1,len(Tablero)):
            if Tablero[i] == Tablero[j]:
                Atacadas += 1
            Dif = j-i
            if Tablero[i] == Tablero[j] - Dif or Tablero[i] == Tablero[j] + Dif:
                Atacadas+= 1
    return N*(N-1)/2 - Atacadas

def Cruce(N):
    Pareja = Parejas(N)
    Temp = 0
    for k,v in Pareja.items():
        if Temp % 2 == 0 :
            Punto = random.randint(1,N-2)
            Padre1 = Individuos[k]
            Padre2 = Individuos[v]
            Hijo1 = []
            Hijo2 = []

            Hijo1.extend(Padre1[0:Punto])
            Hijo1.extend(Padre2[Punto:])

            Hijo2.extend(Padre2[0:Punto])
            Hijo2.extend(Padre1[Punto:])

            Individuos[k] = Hijo1
            Individuos[v] = Hijo2
        Temp += 1
    Mostrar(N)
def Mostrar(N):
    for i in range(N):
        print(Individuos[i], 'Fx', Idoneidad(Individuos[i],N))
N= 8
Inicializacion(N)
print("------------------------------------------------------------------------------")
Seleccion(N)
print("------------------------------------------------------------------------------")
Cruce(N)
print("------------------------------------------------------------------------------")
