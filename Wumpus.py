def conjuncion(p,q):
    R = (p and q)
    return R
def implicacion(p,q):
    if(p == False):
        R = True
    else:
        R = q
    return R

def negacion(p):
    if p == True:
        R = False
    else:
        R = True
    return R

def disyuncion(p,q):
    R = (p or q)
    return R

def dobleImplicacion(p,q):
    if p == q:
        R = True
    else:
        R = False
    return R


if __name__ == '__main__':
    Tabla = []
    p = [False, True]
    q = [False, True]

    for valoresP in p:
        for valoresQ in q:
            for valoresR in p:
                for valoresS in q:
                    for valoresT in p:
                        for valoresU in q:
                            for valoresV in p:
                                Lista= [valoresP, valoresQ, valoresR, valoresS, valoresT, valoresU, valoresV]
                                Tabla.append(Lista)
    i=1
    for (p,q,r,s,t,u,v) in Tabla:
        R1 = negacion(r)
        R2 = dobleImplicacion(p,disyuncion(s,t))
        R3 = dobleImplicacion(q,disyuncion(r,disyuncion(u,v)))
        R4 = negacion(p)
        R5 = q
        BC = conjuncion(R1,conjuncion(R2,conjuncion(R3,conjuncion(R4,R5))))
        print(i," ",p," ",q," ",r," ",s," ",t," ",u," ",v,"|",R1," ",R2," ",R3," ",R4," ",R5," ",BC)
        i += 1