from kanren import run, var,eq
from kanren import Relation, facts
from kanren import *
from random import randint
A = var()
M = var()
P = var()
H = var()
T = var()
Esposos = Relation()
Hijo = Relation()
Diferentes = Relation()
##base de conocimiento

facts( Esposos,("sabino","rosalia"),("david","flor"),("rene","nolith"))

facts(Hijo,("david","sabino"),("rene","sabino"),("hernan","sabino"),
           ("angie","david"),("marcelo","rene"))


##Motor de Inferencia
#Tios
J = run(0,(H,T),Hijo(H,P),Hijo(P,A),Hijo(T,A))
R = run(0, (H,M),Hijo(H,P),Esposos(P,M))
K = run(0, (H,P),Hijo(H,P))
for i in J:
    if i not in K:
        print(i)

#Hermanos
H = run(0,(H,T),Hijo(H,A),Hijo(T,A))
for i in H:
    if i[0] != i[1]:
        print(i)
#print(R)
