def conjuncion(p,q):
    R=(p and q)
    return R
def implicacion(p,q):
    if(p==False):
        R = True
    else:
        R=q
    return R
def negacion(p):
    return not(p)
def disyuncion(p,q):
    R=(p or q)
    return R
def doblecondicional(p,q):
    if (p==q):
        R=True
    else:
        R=False
    return R
if __name__=='__main__':
    Tabla=[]
    p=[False,True]
    q=[False,True]
    r=[False,True]
    x=[False,True]
    y=[False,True]
    z=[False,True]
    w=[False,True]
    for valores_P in p:
        for valores_Q in q:
            for valores_R in r:
                for valores_x in x:
                    for valores_y in y:
                        Lista=[]
                        Lista.append(valores_P)
                        Lista.append(valores_Q)
                        Lista.append(valores_R)
                        Lista.append(valores_x)
                        Lista.append(valores_y)
                        Tabla.append(Lista)
    i=1
    print("#    p    q   x   y   z   w    negac, disyun, conjun, implic, doble, BC")
    for (p,q,r,x,y) in Tabla:
        R1 = implicacion(p, q)
        R2 = implicacion(conjuncion(p, q), r)
        R3 = implicacion(p, implicacion(r, negacion(x)))
        R4 = disyuncion(x, y)

        print(i," ",R1," ",R2," ",R3," ",R4," |",conjuncion(R1,conjuncion(R2,conjuncion(R3,R4))))
        i=i+1


    a=[True,1]
    b=[True,2]
    c=[True,3]
    d=[True,4]
    e=[True,5]
